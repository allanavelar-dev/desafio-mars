/**
 * @author allan avelar
 * Load modules and helpers
 */
var fs = require('fs'),
	readLine = require('readline'),
	logic = require('./helper.js');
	
function run(file, next) {
	
	var reader = readLine.createInterface({
		input : fs.createReadStream(file),
		terminal : false
	});
	
	/*
	** Events
	*/
	reader.on('line', function(line) {
		logic.balanca(line.trim());
	});
};

run(process.argv.length > 2 ? process.argv.slice(2).toString() : './input.txt');

process.on('uncaughtException', function(err) {
	console.log('uncaughtException: ' + err);
});