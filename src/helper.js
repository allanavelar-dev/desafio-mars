/**
 * Default test boxes */
var minBoxes = 10;

function puts(str) {
	console.log(str);
}

exports.puts = this.puts;
exports.balanca = function(input) {
	var boxBalance = null,
		result = 0,
		count = 0;
	
	!input ? boxBalance = [10, 10, 20, 10, 10, 10, 10, 10, 10, 10] : boxBalance = input.split(' ');
	
	if (boxBalance.length != minBoxes)
		return puts("Invalid input");

	/**
	 * Remove 1% from each box
	 */
	for (var i = 0; i < boxBalance.length; i++) {
		boxBalance[i] -= i * (boxBalance[i] * .1);
		count += boxBalance[i];
	}
	count = count % 45;
	result = count > 0 ? minBoxes - count : count;
	
	return result == 0 ?
		puts("Não há caixa com moedas de 2g") :
		puts("A caixa com moedas de 2g é a número " + ((minBoxes + result) + 1));
};