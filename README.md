# Desafio
### Caixas da Mars

Existem 10 caixas cada uma com 10 moedas.

Em uma das caixas cada moeda pesa 2 gramas e nas outras 1 grama.

Há uma balança capaz de pesar todas as 10 caixas de uma vez caso você deseje e é possível mover as moedas de uma caixa para outra.

# Perguta?
Como descobrir em qual caixa estão as moedas de 2 gramas com apenas uma pesagem?

##### Test input:
```javascript
20 10 10 10 10 10 10 10 10 10
10 20 10 10 10 10 10 10 10 10
10 10 20 10 10 10 10 10 10 10
10 10 10 20 10 10 10 10 10 10
10 10 10 10 20 10 10 10 10 10
10 10 10 10 10 20 10 10 10 10
10 10 10 10 10 10 20 10 10 10
10 10 10 10 10 10 10 20 10 10
10 10 10 10 10 10 10 10 20 10
10 10 10 10 10 10 10 10 10 20
```

##### Test Output:
```javascript
A caixa com moedas de 2g é a número 1
A caixa com moedas de 2g é a número 2
A caixa com moedas de 2g é a número 3
A caixa com moedas de 2g é a número 4
A caixa com moedas de 2g é a número 5
A caixa com moedas de 2g é a número 6
A caixa com moedas de 2g é a número 7
A caixa com moedas de 2g é a número 8
A caixa com moedas de 2g é a número 9
A caixa com moedas de 2g é a número 10
```

# Como executar
1) É necessário ter o [Node.js](https://nodejs.org/) instalado no computador.
2) Abra o terminal e vá para a pasta "src" do projeto.
3) Execute o comando:
```javascript
node solucao.js input.txt
```